#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <queue>
#include <chrono>
#include <mutex>
#include <thread>
#include <exception>

using std::string;
using std::cout;
using std::cin;
using std::vector;
using std::find;
using std::endl;
using std::ofstream;
using std::ifstream;
using std::queue;
using std::getline;
using std::condition_variable;
using std::thread;
using std::exception;
using std::mutex;


class MessagesSender
{
private:
	vector<string> _users;
	queue<string> _messages;
	mutex mtx;
	condition_variable cond;
public:
	MessagesSender();
	~MessagesSender();
	int menuAndChoice() const;
	void signin();
	void signout();
	void connectedUsers() const;
	void readFromData();
	void sendToAll();
};
