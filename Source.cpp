#include "MessagesSender.h"

int main()
{
	int choice = 0;
	MessagesSender a;

	do
	{
		system("cls");
		choice = a.menuAndChoice();
		try //Sends to the needed function
		{
			switch (choice)
			{
			case 1:
				a.signin();
				break;
			case 2:
				a.signout();
				break;
			case 3:
				a.connectedUsers();
				system("pause");
				break;
			default:
				break;
			}
		}
		catch (string e)
		{
			cout << e;
		}
		
	} while (choice != 4);
	//a.readFromData();
	//a.sendToAll();
	while (true)
	{ //Makes 2 threads
		thread t1(&MessagesSender::readFromData, &a);
		thread t2(&MessagesSender::sendToAll, &a);
		//Wait till they both finished
		t1.join();
		t2.join();
	}

	system("pause");
}