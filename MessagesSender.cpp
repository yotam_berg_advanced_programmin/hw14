#include "MessagesSender.h"


MessagesSender::MessagesSender()
{
}


MessagesSender::~MessagesSender()
{
}

/*
prints and gets choice
*/
int MessagesSender::menuAndChoice() const
{
	unsigned int choice = 0;
	do
	{
		cout << "1.\tSignin\n";
		cout << "2.\tSignout\n";
		cout << "3.\tConnected Users\n";
		cout << "4.\tExit\n";

		cin >> choice;

	} while (choice > 4 || choice < 1);

	return choice;
}

/*
Signin option
*/
void MessagesSender::signin()
{
	string name;

	cout << "Enter your name: ";
	cin >> name;
	getchar();

	if (find(_users.begin(), _users.end(), name) != _users.end())
	{
		cout << "Name already exists!\n" << endl;
		system("pause");
		system("CLS");
	}
	else
	{
		_users.push_back(name);
	}
}

/*
Signout option
*/
void MessagesSender::signout()
{
	string name;
	vector<string>::iterator it;
	cout << "Enter your name: ";
	cin >> name;
	getchar();

	it = find(_users.begin(), _users.end(), name);
	if (it != _users.end())
	{
		_users.erase(it);
	}
	else
	{
		cout << "Name dose not exists\n" << endl;
		system("pause");
		system("cls");
	}
}

/*
Prints the connected users
*/
void MessagesSender::connectedUsers() const
{
	unsigned int i = 0;

	for (; i < _users.size(); i++)
	{
		cout << i + 1 << "-\t " << _users[i] << endl;
	}
}

/*
Reads the data from the file into the messages
*/
void MessagesSender::readFromData() 
{
	string curr;

	ifstream data;
	string path = "data.txt";
	data.open(path);

	if (data.is_open())
	{
			std::unique_lock<mutex> lck(mtx);
			while (data && getline(data, curr))
			{
				_messages.push(curr);
			}
			lck.unlock();
			cond.notify_one();
	}
	data.close(); //So you could write on it
	//std::this_thread::sleep_for(std::chrono::seconds(60));
}

/*
Sends update meesage to all connected users
*/
void MessagesSender::sendToAll()
{
	int i = 0, j = 0, size = 0;
	string curr;

	ofstream output("output.txt");
	
	std::unique_lock<std::mutex> lckUsers(_mUsers, std::defer_lock);
	while (true)
	{

		std::unique_lock<std::mutex> lckMessages(_mMessages);
		if (_messages.empty())
			_cvMessages.wait(lckMessages); //Waits until a message is added

		lckMessages.unlock();

		if (!output.is_open())
		{
			throw "Error opening file!\n";
		}
		size = _messages.size();
		
		lckMessages.lock(); //Going to use it
		for (i = 0; i < size; i++)
		{
			curr = _messages.front();
			_messages.pop();
			lckMessages.unlock(); //Stoped using it

			lckUsers.lock();//Lock user queue
			for (j = 0; j < _users.size(); j++)
			{
				output << _users[j] << ": " << curr << endl ;
			}
			lckUsers.unlock();
		}
		
}


