#include "threads.h"

mutex mu;

void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}
void call_I_Love_Threads()
{
	thread t1(I_Love_Threads);
	t1.join();
}

/*
Prints the numbers vector
*/
void printVector(vector<int> primes)
{
	for (unsigned int i = 0; i < primes.size(); i++)
	{
		cout << primes[i] << endl;
	}
}

/*
Enters primes to the vector
*/
void getPrimes(int begin, int end, vector<int>& primes)
{
	int i = begin;
	int j = 0;
	bool isPrime = true;

	for (; i <= end; i++)
	{
		isPrime = true;
		for (j = 2; j <= sqrt(i); j++)
		{
			if (!(i%j))
			{
				isPrime = false;
			}
		}
		if (isPrime)
		{
			primes.push_back(i);
		}
	}
}

/*
Calls and calculate time
*/
vector<int> callGetPrimes(int begin, int end)
{
	clock_t start = clock();

	vector<int> v;
	thread t(getPrimes, begin, end, std::ref(v));

	t.join();

	cout << "The function took: " << clock() - start << endl;

	return v;
}

/*
Writes the numbers into a file
*/
void writePrimesToFile(int begin, int end, ofstream& file)
{
	int i = begin;
	int j = 0;
	bool isPrime = true;

	for (i = begin; i <= end; i++)
	{
		isPrime = true;
		for (j = 2; j <= sqrt(i); j++)
		{
			if (!(i%j))
			{
				isPrime = false;
			}
		}
		if (isPrime)
		{
			mu.lock();
			file << i << endl;
			mu.unlock();
		}
	}
}

/*
Uses previous functions to do the task with multiple threads
*/
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	clock_t start = clock();
	ofstream myfile(filePath);
	vector<thread> myThreads;

	int numOfThreads = N;
	
	int segment = (end - begin) / N;

	for (int i = 0; i < numOfThreads; i++)
	{
		myThreads.push_back(thread(writePrimesToFile, i * segment, i * segment + segment, std::ref(myfile)));
	}
	for (int i = 0; i < myThreads.size(); i++)
	{
		myThreads[i].join();
	}

	cout << "The function took: " << clock() - start << endl;
}
